-- TP arbres

import Test.QuickCheck

-- ARBRES BINAIRES

-- Question 1
data Arbre coul val = Noeud coul val (Arbre coul val) (Arbre coul val) | Feuille
  deriving (Show)



-- Question 2
mapArbre :: (coul -> coul) -> (val -> val) -> Arbre coul val -> Arbre coul val
mapArbre _ _ Feuille = Feuille
mapArbre fc fv (Noeud coul val l r) = Noeud (fc coul) (fv val) (mapArbre fc fv l) (mapArbre fc fv r)



-- Question 3 : Sans fonction générique
hauteur1 :: Arbre c v -> Int
hauteur1 Feuille = 0
hauteur1 (Noeud _ _ l r) = 1 + max (hauteur1 l) (hauteur1 r)

taille1 :: Arbre c a -> Int
taille1 Feuille = 0
taille1 (Noeud _ _ l r) = 1 + taille1 l + taille1 r



-- Question 4 : Avec fonction générique
-- dimension :: Num a => (a -> a -> a) -> a -> Arbre coul val -> a
-- dimension _ depart Feuille = depart
-- dimension op n (Noeud _ _ l r) = 1 + (op (dimension op n l) (dimension op n r))

-- hauteur :: Arbre coul a -> Int
-- hauteur = dimension max 0

-- taille :: Arbre coul a -> Int
-- taille = dimension (+) 0

-- Element cours :
dimension :: (coul -> val -> b -> b -> b) -> b -> Arbre coul val -> b
dimension _ n Feuille         = n
dimension f n (Noeud c x l r) = f c x (dimension f n l) (dimension f n r)

hauteur2 :: Integral n => Arbre coul val -> n
hauteur2 = dimension (\_ _ g d -> 1 + max g d) 0

taille2 :: Integral n => Arbre coul val -> n
taille2 = dimension (\_ _ g d -> 1 + g + d) 0



-- Question 5
peigneGauche :: [(coul, val)] -> Arbre coul val
peigneGauche []            = Feuille
peigneGauche ((c, a) : xs) = Noeud c a (peigneGauche xs) Feuille



-- Question 6
-- | prop_hauteurPeigne vérifie si la hauteur du tableau est égale au nombre de noeud max entre la racine et un feuille
prop_hauteurPeigne1 :: [(coul, val)] -> Bool
prop_hauteurPeigne1 xs = length xs == hauteur1 (peigneGauche xs)

prop_hauteurPeigne2 :: [(coul, val)] -> Bool
prop_hauteurPeigne2 xs = length xs == hauteur2 (peigneGauche xs)



-- Question 7
-- | prop_hauteurPeigne vérifie si la taille du tableau est égale à la taille totale de l'arbre
prop_taillePeigne1 :: [(coul, val)] -> Bool
prop_taillePeigne1 xs = length xs == taille1 (peigneGauche xs)

prop_taillePeigne2 :: [(coul, val)] -> Bool
prop_taillePeigne2 xs = length xs == taille2 (peigneGauche xs)

-- listPeigneGauche transforme un peigne à gauche en list
listPeigneGauche :: Arbre a b -> [(a, b)]
listPeigneGauche Feuille = []
listPeigneGauche (Noeud c a l _) =  (c,a) : listPeigneGauche l

-- Verifie que le peigne à gauche correspond à la bonne liste
-- signature trouvé grâce à ghci
prop_listPeigneGauche :: (Eq c, Eq v) => [(c, v)] -> Bool
prop_listPeigneGauche xs = xs == listPeigneGauche(peigneGauche xs)

-- Verifie si mapArbre map correctement l'arbre sonnée en paramètre
-- signature trouvé grâce à ghci
prop_mapPeigne :: (Eq c, Eq v, Num c, Num v) => [(c, v)] -> Bool
prop_mapPeigne xs = map (\(a,b) -> (a+3, b+2)) xs == listPeigneGauche(mapArbre (+3) (+2) (peigneGauche xs))



-- Question 8
estComplet1 :: Arbre coul val -> Bool
estComplet1 Feuille = True
estComplet1 (Noeud _ _ g d) | taille2 g == taille2 d = estComplet1 g && estComplet1 d
                            | otherwise              = False

-- | Retourne True si l'arbre est complet
prop_completPeigne1 :: [(c, v)] -> Bool
prop_completPeigne1 []  = True  == estComplet1 (peigneGauche [])
prop_completPeigne1 [x] = True  == estComplet1 (peigneGauche [x])
prop_completPeigne1 xs  = False == estComplet1 (peigneGauche xs)



-- Question 9 : Pas OK
-- cf README.md

-- estComplet2 :: Arbre c a -> Bool
-- estComplet2 Feuille = True
-- estComplet2 a = dimension (\_ _ g d -> g && d) True a



-- Question 10
-- cf README.md


-- Question 11
complet :: Int -> [(coul, val)] -> Arbre coul val
complet 0 _ = Feuille
complet 1 ((c,v):_) = Noeud c v Feuille Feuille
complet h xs = (\(c,v) -> Noeud c v g d)(xs !! ((2^(h - 1)) - 1))
  where 
    g = complet (h - 1) (take (2^(h - 1) - 1) xs)
    d = complet (h - 1) (drop (2^(h - 1)) xs)


          


-- Question 12
-- Donne une liste contenant une infinité de fois son argument
infinite :: a -> [a]
infinite a = a : infinite a

infinite' :: a -> [a]
infinite' = iterate (\x -> x)



-- Question 13
-- Crée la liste contenant [((),'a'), ((),'b'), ...]
unicodeList :: [((), Char)]
unicodeList = [((), u) | u <- ['a'..]]

complet4 :: Arbre () Char
complet4 = complet 4 unicodeList

-- Question 14
-- Calcule la liste des paires couleur-valeur présentes dans les nœuds de l’arbre
aplatit :: Arbre c a -> [(c, a)]
aplatit Feuille          = []
aplatit (Noeud c v g d)  =  aplatit g ++ [(c,v)] ++ aplatit d


prop_aplatit :: Bool
prop_aplatit = map snd (aplatit complet4) == "abcdefghijklmno"



-- Question 15
-- Calcule si un élément de valeur donnée est présent dans l’arbre
-- element :: Eq a => a -> Arbre c a -> Bool
-- element el a = val `elem` (map snd (aplatit a))

element :: Eq a => a -> Arbre c a -> Bool
element _ Feuille                      = False
element el (Noeud _ v l r) | el == v   = True
                           | otherwise = element el l || element el r



-- AFFICHAGE DES ARBRES

-- Question 16
-- renvoie une couleur sous cette forme : [color=red, fontcolor=red]  
afficheC :: (Show a) => a -> String
afficheC c = "[color=" ++ show c ++ ", fontcolor=" ++ show c ++ "]"

-- renvoie la valeur sous forme de chaîne de caractére (Generique)
afficheG :: (Show a) => a -> String
afficheG = show 

-- renvoie l'affichage d'un noeud
noeud :: (c -> String) -> (a -> String) -> (c,a) -> String
noeud aC aV (c, v) = aV v ++ aC c



-- Question 17
-- Renvoie la valeur d'un arbre s'il en a une sinon erreur
valeur :: Arbre c v -> v
valeur Feuille         = error "Pas de valeur"
valeur (Noeud _ v _ _) = v

-- Renvoie la liste des arcs d'un arbre
arcs :: Arbre c a -> [(a,a)]
arcs Feuille                     = []
arcs (Noeud _ _ Feuille Feuille) = []
arcs (Noeud _ v l Feuille)       = (v, valeur l) : arcs l
arcs (Noeud _ v Feuille r)       = (v, valeur r) : arcs r
arcs (Noeud _ v l r)             = (v, valeur l) : (v, valeur r) : arcs l ++ arcs r



-- Question 18
-- Renvoie l'affiche d'un arc
arc :: (a -> String) -> (a,a) -> String
arc afficheA (n1, n2) = afficheA n1 ++ " -> " ++ afficheA n2



-- Question 19
-- Renvoie l'affiche d'un arbre
dotise :: (Show a) => String -> (c -> String) -> (a -> String) -> Arbre c a -> String
dotise nom fc fv arbre = "digraph "++ nom ++ " {\n"
                      ++ "node [fontname=\"DejaVu-Sans\", shape=circle]\n"
                      ++ unlines (map (\n -> noeud fc fv n) (aplatit arbre))
                      ++ unlines (map (\a -> arc show a) (arcs arbre))
                      ++ "}"



-- ENFIN DE LA COULEUR

-- Question 20
-- signature trouvé avec ghci
elementR :: Ord a => a -> Arbre coul a -> Bool
elementR _ Feuille = False
elementR x (Noeud _ v l r) | x < v     = elementR x l
                           | x > v     = elementR x r
                           | otherwise = True



-- Question 21
data Couleur = R | N
  deriving Show

type ArbreRN n = Arbre Couleur n


-- Question 22
equilibre :: ArbreRN a -> ArbreRN a
equilibre (Noeud N vz (Noeud R vy (Noeud R vx a b) c) d) = Noeud N vy (Noeud N vx a b) (Noeud N vz c d)
equilibre (Noeud N vz (Noeud R vx a (Noeud R vy b c)) d) = Noeud N vy (Noeud N vx a b) (Noeud N vz c d)
equilibre (Noeud N vx a (Noeud R vz (Noeud R vy b c) d)) = Noeud N vy (Noeud N vx a b) (Noeud N vz c d)
equilibre (Noeud N vx a (Noeud R vy b (Noeud R vz c d))) = Noeud N vy (Noeud N vx a b) (Noeud N vz c d)
equilibre a = a


-- Question 23
rTon :: ArbreRN n -> ArbreRN n
rTon Feuille = Feuille
rTon (Noeud _ v l r) = Noeud N v l r


insertion :: Ord a => a -> ArbreRN a -> ArbreRN a
insertion vajout arbre = rTon (go vajout arbre)
  where go v a | elementR v a = a
        go v Feuille = Noeud R v Feuille Feuille
        go v (Noeud c varbre l r) | v < varbre = equilibre(Noeud c varbre (insertion v l) r)
                                  | v > varbre = equilibre(Noeud c varbre l (insertion v r))


-- Question 24

-- Question 25

-- Question 26