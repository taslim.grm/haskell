somme :: [Int] -> Int
somme [] = 0
somme (x:xs) = x + somme(xs)

last_ :: [a] -> a
last_ [] = error "empty list"
last_ [x] = x
last_ (x:xs) = last_ xs


init_ :: [a] -> [a]
init_ [] = []
init_ [x] = []
init_ (x:xs) = x : (init_ xs) 

-- = [x] ++ (init_)  équivalent
-- ajout par la gauche dans une liste a droite (reviens a .append par la gauche) ==> x : x : [] = [x,x]

from_index :: Int -> [a] -> a
from_index 0 (x:xs) = x
from_index i (x:xs) = from_index (i-1) xs

add_lists :: [a] -> [a] -> [a]
add_lists xs (y:ys) = add_lists (y:xs) ys
add_lists xs [] = xs

-- last_ :: [a] -> a
-- last_ xs =  xs !! ((length xs) - 1)

-- init_ :: [a] -> [a]
-- init_ xs = take ((length xs) - 1) xs

