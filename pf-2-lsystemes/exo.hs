type Symbole = Char
type Mot = [Symbole]
type Axiome = Mot
type Regles = Symbole -> Mot
type LSysteme = [Mot]

motSuivant :: Regles -> Mot -> Mot
motSuivant _ [] = []
motSuivant r (x:xs) = r(x) ++ motSuivant r xs

-- RegleFlocon :: Symbole -> Mot


-- lsysteme :: Axiome -> Regles -> LSysteme
-- lsysteme 