import Graphics.Gloss

type Symbole = Char
type Mot = [Symbole]
type Axiome = Mot
type Regles = Symbole -> Mot
type LSysteme = [Mot]

type EtatTortue = (Point, Float)
type Config = (EtatTortue -- État initial de la tortue
              ,Float      -- Longueur initiale d’un pas
              ,Float      -- Facteur d’échelle
              ,Float      -- Angle pour les rotations de la tortue
              ,[Symbole]) -- Liste des symboles compris par la tortue
type EtatDessin = (EtatTortue, Path)
-- type Path = [Point]

-- Partie 1 

motSuivant :: Regles -> Mot -> Mot
motSuivant _ [] = []
motSuivant r (x:xs) = r(x) ++ motSuivant r xs

regleFlocon :: Symbole -> Mot
regleFlocon 'F' = "F-F++F-F"
regleFlocon x = [x]

lsysteme :: Axiome -> Regles -> LSysteme
lsysteme ax r = iterate (motSuivant r) ax 

-- Partie tortue

etatInitial :: Config -> EtatTortue
etatInitial (x,_,_,_,_) = x 

longueurPas :: Config -> Float
longueurPas (_,x,_,_,_) = x

facteurEchelle :: Config -> Float
facteurEchelle (_,_,x,_,_) = x

angle :: Config -> Float
angle (_,_,_,x,_) = x

symbolesTortue :: Config -> [Symbole]
symbolesTortue (_,_,_,_,x) = x

-- déplacements

avance :: Config -> EtatTortue -> EtatTortue -- F
avance c ((x,y),cap) = ((newX,newY),cap)
    where
        newX = x + (longueurPas c) * (cos cap)
        newY = y + (longueurPas c) * (sin cap)

tourneAGauche :: Config -> EtatTortue -> EtatTortue -- +
tourneAGauche c ((x,y),cap) = ((x,y),newCap)
    where
        newCap = (cap + (angle c)) --`mod` 360

tourneADroite :: Config -> EtatTortue -> EtatTortue -- -
tourneADroite c ((x,y),cap) = ((x,y),newCap)
    where
        newCap = (cap - (angle c)) --`mod` 360

isMember :: Eq a => a -> [a] -> Bool 
isMember n [] = False
isMember n (x:xs) | (x == n) = True
isMember n (x:xs) = isMember n xs

filtreSymbolesTortue :: Config -> Mot -> Mot
filtreSymbolesTortue c [] =  []
filtreSymbolesTortue c (x:xs) | isMember x ( symbolesTortue c ) =  x : ( filtreSymbolesTortue c xs) -- x ∈ ( symbolesTortue c )
filtreSymbolesTortue c (x:xs) = filtreSymbolesTortue c xs

interpreteSymbole :: Config -> EtatDessin -> Symbole -> EtatDessin
interpreteSymbole c (et, p) symb = (newet, newp) where
    newet = (func_from_symb symb) c et where
        func_from_symb _s | (_s == '+') = tourneAGauche
        func_from_symb _s | (_s == '-') = tourneADroite
        func_from_symb _s | (_s == 'F') = avance
    newp = (get_coord et) : p where
        get_coord (_et, _p) = _et

interpreteMot :: Config -> Mot -> Picture
interpreteMot c m = (line path) where
    path = get_path (develop_path m ((etatInitial c) []) ) where
        get_path (_etat_tortue, _path) = _path
        develop_path (x:xs) etat_dessin = develop_path xs (interpreteSymbole c etat_dessin x)
        develop_path [] etat_dessin = etat_dessin